$package_exist =(choco search fing --version ~version~ | Select -last 1)
if ($package_exist -eq '0 packages found.') { 
	Write-Host "Pushing the package v~version~"
	choco push fing.~version~.nupkg --api-key $tracker_bowlman -Source https://tracker.bowlman.be/upload
	choco push fing.~version~.nupkg --api-key $choco_api -Source https://push.chocolatey.org
} else {
	Write-Host "Package already available, skipping"
}