param (
    [string]$url = $(throw "-url is required.")
)

Write-host "Downloading"
Invoke-WebRequest -Uri $url -OutFile ./fing.exe

Write-host "Installation"
./fing.exe /S

Write-host "Get version number"
$fing=$(tools/Get-InstalledApps.ps1 -ComputerName $env:COMPUTERNAME -NameRegex 'fing')
while($fing.count -eq 0)
{
	$fing=$(tools/Get-InstalledApps.ps1 -ComputerName $env:COMPUTERNAME -NameRegex 'fing')
}
$version=$fing.DisplayVersion
$version
$sha1=$(checksum "./fing.exe" -t sha1)
tools/Replace-FileString.ps1 "~url~" $url fing/tools/chocolateyInstall.ps1 -Overwrite
tools/Replace-FileString.ps1 "~sha1~" $sha1 fing/tools/chocolateyInstall.ps1 -Overwrite
tools/Replace-FileString.ps1 "~version~" $version fing/fing.nuspec -Overwrite
tools/Replace-FileString.ps1 "~version~" $version .gitlab-ci.yml -Overwrite
tools/Replace-FileString.ps1 "~version~" $version upload.ps1 -Overwrite

&wmic product where "name='fing' " uninstall
